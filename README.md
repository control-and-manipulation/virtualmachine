# README #

This is an image of Ubuntu 18.04 with the basic setup fully installed. They include updates up to 09/11/2021.
Link to image: https://www.dropbox.com/s/nu9benm9hi429dt/Ubuntu18Base.ova?dl=0

User name: **TiiControl**

password: **123**


## Its major components are:
- ros melodic full installation 
- trajectoy controller package
- joint state publisher package
- moveit package
- universal robot drivers
- moveit tutorials

**The directory ~/control_ws contains moveit tutorials and the universal robot code.
ros is automatically sourced from within bashrc.**

## Other components included:
- terminator
- vim
- tweak tool
- snapd
- vlc
- gimp
- visual studio code
- sublime text
- peek (a screen recording software)
- skype
- telegram desktop
- blender - an open source 3D creation software
- synaptic - graphical package management tool
- android studio
- pycharm
- slack
- spotify
- discord